# Manage users nginx with lua and redis
#### 1. Tổng quan
- Trước khi muốn vào từng service, người dùng phải auth bằng gmail
- Thông qua gmail, redis lấy được services, role của service. check giá trị và cho phép user truy cập vào service đó với role của mình
- Nếu gmail chưa được auth thì sẽ báo lỗi 405 và quay về trang auth 
#### 2. Routing
- /auth : Dùng để auth 
- /services/gmail : Khi auth thành công sẽ redirect và show các service của gmail đó
- /service/name-service/gmail : Đường dẫn để service của gmail đó được access
- /access/name-service/gmail : Service được gọi về để access sẽ chuyển về đây 
- /405.html : Lỗi auth của web
- /401.html : Lỗi method cho phép
- /block.html : User đã bị block
- /error.html : Lỗi hệ thống hoặc lỗi url request
#### 3. Yêu cầu 
- Cần cài docker, docker-compose.
#### 4. Port 
- 80808:80
#### 5. Cách dùng 
> docker-compose up -d --build 
