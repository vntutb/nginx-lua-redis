local _M = {}
local cjson = require "cjson"
local redis = require "resty.redis" 
function _M.conectionRedis()
    local red = redis:new()
    red:set_timeouts(1000, 1000, 1000) 
    local ok, err = red:connect("192.168.48.2", 6379)
    if not ok then
        ngx.say("failed to connect: ", err)
        return
    end
    return red
end
function _M.checkBlock(block)
    return block
end
function _M.checkResult(res)
    if type(res) == "string" then
        return true
    else
        return false
    end
end
function _M.decodeResult(res)
    return cjson.decode(res)
end
function _M.index()
    local red = _M.conectionRedis()
    local gmail = "vntutb@gmail.com"
    local info = ngx.shared.info
    info:set("auth-gmail",gmail)
    local res, err = red:get(gmail)
    if _M.checkResult(res) then
        ngx.var.target = string.format('/services/%s',gmail);
    else
        ngx.var.target = '/401.html';
    end
    ngx.redirect(ngx.var.target)
end
function _M.access()
    local red = _M.conectionRedis()
    local res, err = red:get(ngx.var.email)
    local message = cjson.decode(res)
    local role
    if message[ngx.var.service] ~= null then
        role = message[ngx.var.service].Role
    end
    local roleAccess
    if role == 'read' then
        roleAccess = 1;
    end
    if role == nil then
        local uri_service ='';
        uri_service ='/error.html'
        ngx.redirect(uri_service)
    end
end
function _M.services()
    local uri = ngx.var.uri
    local uriArr = {}
    for word in string.gmatch(uri, "[^/]+") do
        table.insert(uriArr, word)
    end
    local info = ngx.shared.info;
    local value, flags = info:get("auth-gmail")
    if value ~= uriArr[2] then
        local uri_service ='';
        uri_service ='/401.html'
        ngx.redirect(uri_service)
    end
end
function _M.service()
    local info = ngx.shared.info
    local http = require "resty.http"
    local httpc = http.new()
    local url_request =string.format('http://192.168.48.4:3000%s',ngx.var.uri);
    -- ngx.say(ngx.var.uri)
    local res, err = httpc:request_uri(url_request, {
        method = "GET",
    })
    if not res then
        ngx.log(ngx.ERR, "request failed: ", err)
        return
    end
    local emailName = res.headers["Email"] 
    local serviceName = res.headers["Service"] 
    local role = res.headers["Role"] 
    local uri_service ='';
    info:set("role",role)
    info:set("service", serviceName);
    info:set("email",emailName)
    local value, flags = info:get("auth-gmail")
    if value ~= emailName then
        uri_service ='/401.html'
    else
        uri_service =string.format("/access/%s/%s/",serviceName,emailName)
    end
    ngx.redirect(uri_service)
end
return _M